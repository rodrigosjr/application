package Model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Status.class)
public abstract class Status_ {

	public static volatile CollectionAttribute<Status, Ticket> ticket;
	public static volatile SingularAttribute<Status, String> description;
	public static volatile SingularAttribute<Status, Integer> id;

}

