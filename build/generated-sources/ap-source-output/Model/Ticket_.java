package Model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Ticket.class)
public abstract class Ticket_ {

	public static volatile SingularAttribute<Ticket, Date> dateClosing;
	public static volatile SingularAttribute<Ticket, String> description;
	public static volatile CollectionAttribute<Ticket, Message> messages;
	public static volatile SingularAttribute<Ticket, Date> dateOpening;
	public static volatile SingularAttribute<Ticket, Long> id;
	public static volatile SingularAttribute<Ticket, String> title;
	public static volatile SingularAttribute<Ticket, User> user;
	public static volatile SingularAttribute<Ticket, Status> status;

}

