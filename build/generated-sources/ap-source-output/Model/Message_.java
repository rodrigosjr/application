package Model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Message.class)
public abstract class Message_ {

	public static volatile SingularAttribute<Message, Date> date;
	public static volatile SingularAttribute<Message, Ticket> ticket;
	public static volatile SingularAttribute<Message, String> description;
	public static volatile SingularAttribute<Message, Integer> id;
	public static volatile SingularAttribute<Message, User> user;

}

