package Model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Hierarchy.class)
public abstract class Hierarchy_ {

	public static volatile SingularAttribute<Hierarchy, String> description;
	public static volatile SingularAttribute<Hierarchy, Long> id;
	public static volatile CollectionAttribute<Hierarchy, User> users;

}

