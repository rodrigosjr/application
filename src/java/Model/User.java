
package Model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user", catalog = "suporte", schema = "")

public class User implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "USER_ID")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "USER_NAME")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "USER_LOGIN")
    private String login;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "USER_PASS")
    private String password;

    @JoinColumn(name = "USER_HIER_ID", referencedColumnName = "HIER_ID")
    @ManyToOne(optional = false)
    private Hierarchy hierarchy;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Collection<Ticket> tickets;

    
    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    public User(Long id, String name, String login, String password) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Hierarchy getHierarchy() {
        return hierarchy;
    }

    public void setHierarchy(Hierarchy hierarchy) {
        this.hierarchy = hierarchy;
    }

    public Collection<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Collection<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public int hashCode() {
        return (this.id != null ? this.id.hashCode() : 0);
    }

    
    
    @Override
    public boolean equals(Object obj) {
        //Verifica se as instâncias pertencem ao mesmo Objeto.
        if (this == obj) {
            return true;
        }
        //Verifica se a variavel faz referencia à  uma instancia de objeto.
        if (obj == null) {
            return false;
        }
        //Verifica se o objeto é uma instancia desta classe.
        if (getClass() != obj.getClass()) {
            return false;
        }
        //Verifica se os Objetos primitivos Long são "iguais".
        return Objects.equals(this.hashCode(), obj.hashCode());
    }

    @Override
    public String toString() {
        return "User{" + "hashCode: " + hashCode() + ", name: " + name + ", login: " + login + ", password: " + password + "}";
    }
    
}



