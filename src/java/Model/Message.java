
package Model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "message", catalog = "suporte", schema = "")

public class Message implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MESS_ID")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "MESS_DT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "MESS_DESC")
    private String description;
    
    @JoinColumn(name = "MESS_TICK_ID", referencedColumnName = "TICK_ID")
    @ManyToOne(optional = false)
    private Ticket ticket;
 
    @JoinColumn(name = "MESS_USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)    
    private User user;

    public Message() {
    }

    public Message(Integer messId) {
        this.id = messId;
    }

    public Message(Integer id, Date date, String description) {
        this.id = id;
        this.date = date;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    @Override
    public int hashCode() {
        return (this.id != null ? this.id.hashCode() : 0);
    }

    @Override
    public boolean equals(Object obj) {
        //Verifica se as instâncias pertencem ao mesmo Objeto.
        if (this == obj) {
            return true;
        }
        //Verifica se a variavel faz referencia à  uma instancia de objeto.
        if (obj == null) {
            return false;
        }
        //Verifica se o objeto é uma instancia desta classe.
        if (getClass() != obj.getClass()) {
            return false;
        }

        //Verifica se os Objetos primitivos Long são "iguais".
        return Objects.equals(this.hashCode(), obj.hashCode());
    }

    @Override
    public String toString() {
        return "Message{" + "hashCode: " + hashCode() + ", date: " + date + ", description: " + description + "}";
    }
    
}
