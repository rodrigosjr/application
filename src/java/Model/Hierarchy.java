
package Model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "hierarchy", catalog = "suporte", schema = "")

public class Hierarchy implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "HIER_ID")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "HIER_DESC")
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "hierarchy")
    private Collection<User> users;

    public Hierarchy() {
    }

    public Hierarchy(Long id) {
        this.id = id;
    }

    public Hierarchy(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        return (this.id != null ? this.id.hashCode() : 0);
    }

    @Override
    public boolean equals(Object obj) {
        //Verifica se as instâncias pertencem ao mesmo Objeto.
        if (this == obj) {
            return true;
        }
        //Verifica se a variavel faz referencia à  uma instancia de objeto.
        if (obj == null) {
            return false;
        }
        //Verifica se o objeto é uma instancia desta classe.
        if (getClass() != obj.getClass()) {
            return false;
        }

        //Verifica se os Objetos primitivos Long são "iguais".
        return Objects.equals(this.hashCode(), obj.hashCode());
    }

    @Override
    public String toString() {
        return "Hierarchy{" + "hashCode: " + hashCode() + ", description: " + description + "}";
    }

}
