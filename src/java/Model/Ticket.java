package Model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "ticket", catalog = "suporte", schema = "")

public class Ticket implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TICK_ID")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "TICK_DT_OPEN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOpening;

    @Basic(optional = false)
    @Column(name = "TICK_DT_CLOS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateClosing;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TICK_TITLE")
    private String title;

    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "TICK_DESC")
    private String description;
    
    @JoinColumn(name = "TICK_USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ticket")
    private Collection<Message> messages;

    @JoinColumn(name = "TICK_STAT_ID", referencedColumnName = "STAT_ID")
    @ManyToOne(optional = false)
    private Status status;

    public Ticket() {
    }

    public Ticket(Long tickId) {
        this.id = tickId;
    }

    public Ticket(Long id, Date dateOpening, Date dateClosing, String title, String status) {
        this.id = id;
        this.dateOpening = dateOpening;
        this.dateClosing = dateClosing;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateOpening() {
        return dateOpening;
    }

    public void setDateOpening(Date dateOpening) {
        this.dateOpening = dateOpening;
    }

    public Date getDateClosing() {
        return dateClosing;
    }

    public void setDateClosing(Date dateClosing) {
        this.dateClosing = dateClosing;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Collection<Message> getMessages() {
        return messages;
    }

    public void setMessages(Collection<Message> messages) {
        this.messages = messages;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        return (this.id != null ? this.id.hashCode() : 0);
    }

    @Override
    public boolean equals(Object obj) {
        //Verifica se as instâncias pertencem ao mesmo Objeto.
        if (this == obj) {
            return true;
        }
        //Verifica se a variavel faz referencia à  uma instancia de objeto.
        if (obj == null) {
            return false;
        }
        //Verifica se o objeto é uma instancia desta classe.
        if (getClass() != obj.getClass()) {
            return false;
        }

        //Verifica se os Objetos primitivos Long são "iguais".
        return Objects.equals(this.hashCode(), obj.hashCode());
    }

    @Override
    public String toString() {
        return "Ticket{" + "hashCode: " + hashCode() + ", dateOpening: " + dateOpening + ", dateClosing: " + dateClosing + ", title: " + title + ", status: " + status + "}";
    }

}
