
package Model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "status", catalog = "suporte", schema = "")

public class Status implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "STAT_ID")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "STAT_DESC")
    private String description;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
    private Collection<Ticket> ticket;

    public Status() {
    }

    public Status(Integer statId) {
        this.id = statId;
    }

    public Status(Integer statId, String statDesc) {
        this.id = statId;
        this.description = statDesc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Ticket> getTicket() {
        return ticket;
    }

    public void setTicket(Collection<Ticket> ticket) {
        this.ticket = ticket;
    }

    @Override
    public int hashCode() {
        return (this.id != null ? this.id.hashCode() : 0);
    }

    @Override
    public boolean equals(Object obj) {
        //Verifica se as instâncias pertencem ao mesmo Objeto.
        if (this == obj) {
            return true;
        }
        //Verifica se a variavel faz referencia à  uma instancia de objeto.
        if (obj == null) {
            return false;
        }
        //Verifica se o objeto é uma instancia desta classe.
        if (getClass() != obj.getClass()) {
            return false;
        }

        //Verifica se os Objetos primitivos Long são "iguais".
        return Objects.equals(this.hashCode(), obj.hashCode());
    }

    @Override
    public String toString() {
        return "Status{" + "hashCode: " + hashCode() + ", description: " + description + "}";
    }
}
