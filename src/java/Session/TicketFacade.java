
package Session;

import Model.Ticket;
import javax.ejb.Stateless;

/**
 *
 * @author Rodrigo
 * 
 * Os metodos para Insert, Update, Delete e Select são herdados da classe AbstractFacace.java
 */
@Stateless
public class TicketFacade extends AbstractFacade<Ticket> {

    public TicketFacade() {
        super(Ticket.class);
    }

    // Implemente aqui os metodos esclusivos para manipular os Objetos Ticket.
      
}
