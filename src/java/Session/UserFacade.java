
package Session;

import Model.User;
import javax.ejb.Stateless;
/**
 *
 * @author Rodrigo
 * 
 * Os metodos para Insert, Update, Delete e Select são herdados da classe AbstractFacace.java
 */
@Stateless
public class UserFacade extends AbstractFacade<User> {

    public UserFacade() {
        super(User.class);
    }
    
}
