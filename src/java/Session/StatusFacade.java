
package Session;

import Model.Status;
import javax.ejb.Stateless;

/**
 *
 * @author Rodrigo
 * 
 * Os metodos para Insert, Update, Delete e Select são herdados da classe AbstractFacace.java
 */
@Stateless
public class StatusFacade extends AbstractFacade<Status> {

    public StatusFacade() {
        super(Status.class);
    }
 
    // Implemente aqui os metodos esclusivos para manipular os Objetos Status.
      
}
