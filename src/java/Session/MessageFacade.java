
package Session;

import Model.Message;
import javax.ejb.Stateless;

/**
 *
 * @author Rodrigo
 * 
 * Os metodos para Insert, Update, Delete e Select são herdados da classe AbstractFacace.java
 */
@Stateless
public class MessageFacade extends AbstractFacade<Message> {

    public MessageFacade() {
        super(Message.class);
    }

    // Implemente aqui os metodos esclusivos para manipular os Objetos Message.
      
}
