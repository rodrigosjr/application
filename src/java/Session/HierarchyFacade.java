
package Session;

import Model.Hierarchy;
import javax.ejb.Stateless;

/**
 *
 * @author Rodrigo
 * 
 * Os metodos para Insert, Update, Delete e Select são herdados da classe AbstractFacace.java
 */
@Stateless
public class HierarchyFacade extends AbstractFacade<Hierarchy> {

    public HierarchyFacade() {
        super(Hierarchy.class);
    }
    
    // Implemente aqui os metodos esclusivos para manipular os Objetos Hierarchy.
    
}
