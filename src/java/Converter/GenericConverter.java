/*
 * Classe utilizada para converter Objeto em String e String em Objeto.
 * Classe utilizada em conjunto com o componente SelectOneMemu do PrimeFaces.
 */
package Converter;

/**
 *
 * @author Rodrigo
 */

import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("genericConverter")
public class GenericConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && value.trim().length() > 0) {
                List objects
                        //Captura a relação de UIComponents (UISelectItem, UISelectItems...).
                        = (List) ((UISelectItems) uic.getChildren()
                        .stream()
                        //Filtra o componente UISelectItems.
                        .filter(UISelectItems -> UISelectItems.getClass() == UISelectItems.class)
                        //Retorna a primeira ocorrencia do componente selecionado.
                        .findFirst()
                        //Efetua o desencapsulamento de <Optional> para <UISelectItems>.
                        .get())
                        //Captura a lista de Objetos carregados no componente UISelectItems.
                        .getValue();

                Object object
                        = objects.stream()
                        //Filtra o objeto selecionado pelo usuário com base no HashCode.
                        .filter(Object -> Object.hashCode() == Long.parseLong(value))
                        //Retorna a primeira ocorrencia do objeto filtrado.
                        .findFirst()
                        //Efetua o desencapsulamento de <Optional> para <Object>        
                        .get();

                return object;
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) throws NullPointerException {
        try {
            //Converte o hashCode do objeto em String e retorna para o SelectOneMenu aplicar o valor no campo value do SelectItems..
            return String.valueOf((object.hashCode()));
        } catch (NullPointerException ex) {
            //throw new UnsupportedOperationException(ex);
            return null;
        }
    }
}
