package Controller;

import Controller.util.Message;
import Session.AbstractFacade;
import Session.Lazy.LazyEntityDataModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

/**
 * @param <T>
 */
public abstract class AbstractController<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private AbstractFacade<T> ejbFacade;
    // Armazena o tipo da classe que extende AbstractController.java
    private final Class<T> item;
    /*
     * Armazena o Objeto selecionado naS tabelas do Front-End.
     * A variavel também é utilizada para armazenar o objeto que receberá os dados dos formulários.
    */
    private T selected;
    private Collection<T> items;
    private LazyEntityDataModel<T> lazyItems;

    private enum PersistAction {
        CREATE,
        DELETE,
        UPDATE
    }

    /*
     * Contrutor que recebe o tipo da classe que extende AbstractController.java
     */
    public AbstractController(Class<T> item) {
        this.item = item;
    }

    /*
     * Instância um novo Objeto para receber os dados dos Formulários.
     * 
     * O objeto é do tipo da classe que extende AbstractController.java
     */
    public void prepareCreate() {
        try {
            this.selected = item.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
     * A anotação @PostConstruct faz com que o metodo seja executado após a
     * conclusão da injeção de dependencia.
     *
     * O metodo com @PostConstruct é invocado depois que o contrutor é chamado.
     *
     * O metodo auxilia na transição entre as paginas, recuperando uma coleção de
     * Objetos disponibilizada previamente no Map da requisição.
     * 
     * Exemplo: Quando o usuário acessa a tela de cadastro de Hierarquias o Objeto
     * Hierarchy é instanciado e tras consigo uma lista de usuários; Se o cliente
     * requisitar a pagina de usuários, a aplicação disponibiliza a lista de Objetos
     * User, contida em Hierarchy, no Map do request, eliminando a necessidade de 
     * uma nova consulta ao banco de dados.
     */
    @PostConstruct
    private void getContextItems() {
        Object itemsList = FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(item.getSimpleName() + "_items");
        if (itemsList != null) {
            setItems((Collection<T>) itemsList);
            setLazyItems((Collection<T>) itemsList);
        }
    }

    public abstract String setContextItems();    
    
    public T getSelected() {
        return selected;
    }

    public void setSelected(T selected) {
        this.selected = selected;
    }
    
    public Collection<T> getItems() {
        if (items == null) {
           setItems(this.ejbFacade.findAll());
        }
        return items;
    }

    /**
     * @param first posição inicial da consulta; Ex: 0.
     * @param pageSize numero maximo de resultados; Ex: 0.
     * @param sortField descrição da coluna, por exemplo, a ser ordenada; Ex: null.
     * @param sortOrder Ascendente ou Descendentente; Ex: null.
     * @param filters mapa com o nome das colunas e valores procurados;Map com o nome da coluna e o valor procurado.
     * @return
     */
    public Collection<T> getItems(int first, int pageSize, String sortField, String sortOrder, Map<String, Object> filters) {
        setItems(this.ejbFacade.findRange(first, pageSize, sortField, sortOrder, filters));
        return items;
    }

    /**
     * @param items
     */
    public void setItems(Collection<T> items) {
        this.items = items;
    }

    /**
     * @return
     */
    public LazyEntityDataModel<T> getLazyItems() {
            lazyItems = new LazyEntityDataModel<>(this.ejbFacade);
        return lazyItems;
    }

    public void setLazyItems(LazyEntityDataModel<T> lazyItems) {
        this.lazyItems = lazyItems;
    }

    public void setLazyItems(Collection<T> items) {
        if (items instanceof List) {
            lazyItems = new LazyEntityDataModel<>((List<T>) items);
        } else {
            lazyItems = new LazyEntityDataModel<>(new ArrayList<>(items));
        }
    }

    /**
     * Encapsula os metodos de persistencia do Pacote Session.
     */
    public void save() {
        String msg = ResourceBundle.getBundle("/Portuguese").getString(item.getSimpleName() + "Created");
        persist(PersistAction.CREATE, msg);
        if (!FacesContext.getCurrentInstance().isValidationFailed()) {
            items = null;
            lazyItems = null;
        }
    }

    public void edit() {
        String msg = ResourceBundle.getBundle("/Portuguese").getString(item.getSimpleName() + "Updated");
        persist(PersistAction.UPDATE, msg);
    }

    public void delete() {
        String msg = ResourceBundle.getBundle("/Portuguese").getString(item.getSimpleName() + "Deleted");
        persist(PersistAction.DELETE, msg);
        if (!FacesContext.getCurrentInstance().isValidationFailed()) {
            selected = null;
            items = null;
            lazyItems = null;
        }
    }

    /**
     * Realiza quaisquer ações de modificação de dados para uma entidade. As
     * ações que podem ser realizadas por meio deste método são controladas pela
     * {@link PersistAction} enumeração.
     *
     * @param persistAction uma ação específica que deve ser realizado no item
     * atual
     *
     * @param successMessage uma mensagem que deve ser exibida quando os dados
     * forem persistidos com sucesso
     */
    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            try {
                switch (persistAction) {
                    case CREATE:
                        this.ejbFacade.create(selected);
                        break;
                    case UPDATE:
                        this.ejbFacade.edit(selected);
                        break;
                    case DELETE:
                        this.ejbFacade.remove(selected);
                        break;
                }
                Message.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                Throwable cause = Message.getRootCause(ex.getCause());
                if (cause != null) {
                    if (cause instanceof ConstraintViolationException) {
                        ConstraintViolationException excp = (ConstraintViolationException) cause;
                        excp.getConstraintViolations().forEach((s) -> {
                            Message.addErrorMessage(s.getMessage());
                        });
                    } else {
                        String msg = cause.getLocalizedMessage();
                        if (msg.length() > 0) {
                            Message.addErrorMessage(msg);
                        } else {
                            Message.addErrorMessage(ex, ResourceBundle.getBundle("/Portuguese").getString("PersistenceErrorOccured"));
                        }
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                Message.addErrorMessage(ex, ResourceBundle.getBundle("/Portuguese").getString("PersistenceErrorOccured"));
            }
        }
    }

}
