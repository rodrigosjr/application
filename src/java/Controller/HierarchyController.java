package Controller;

import Model.Hierarchy;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;


@Named(value = "hierarchyController")
@ViewScoped
public class HierarchyController extends AbstractController<Hierarchy> {

    public HierarchyController() {
        super(Hierarchy.class);
    }

    /**
     * Ao ser instanciado, o objeto Hierarchy.java tras consigo a lista de usuários vinculados a ele.
     * 
     * Quando o cliente está na pagina de cadastro de hierarquias e efetua uma requisição para
     * a pagina de cadastro de usuários, a aplicação inclui no Map de requisição a lista de Objetos
     * User contida no Objeto Hierarchy para que possa ser recuperada em seguida por outro Objeto.
     * 
     * O metodo navigateUsers() auxilia na transição dos dados entre as requsições.
     * 
     * @return link de navegação (outcome) para a pagina User
     */
    
    @Override
    public String setContextItems() {
        if (getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("User_items", getSelected().getUsers());
        }
        return "/index/user/index";
    }

}
