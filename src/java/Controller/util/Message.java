package Controller.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class Message {

    public static void addErrorMessage(Exception ex, String defaultMessage) {
        String msg = ex.getLocalizedMessage();
        if (msg != null && msg.length() > 0) {
            addErrorMessage(msg);
        } else {
            addErrorMessage(defaultMessage);
        }
    }

    public static void addErrorMessage(String message) {
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        //Define um flag que indica que ocorreu um erro de conversão ou validação Ao processar as entradas.
        FacesContext.getCurrentInstance().validationFailed();

    }

    public static void addSuccessMessage(String message) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, message, message);
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);
    }

    public static Throwable getRootCause(Throwable cause) {
        if (cause != null) {
            Throwable source = cause.getCause();
            if (source != null) {
                return getRootCause(source);
            } else {
                return cause;
            }
        }
        return null;
    }

}
