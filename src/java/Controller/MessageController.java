package Controller;

import Model.Message;
import Model.User;
import java.util.Calendar;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "messageController")
@ViewScoped
public class MessageController extends AbstractController<Message> {


    private TicketController ticketController;

    public TicketController getTicketController() {
        return ticketController;
    }

    public void setTicketController(TicketController ticketController) {
        this.ticketController = ticketController;
    }

    public MessageController() {
        super(Message.class);
    }
    
    /* Grava a data atual na propriedade do Objeto Message,
     * atribui o objeto Message na lista de Objstos de mesmo
     * tipo do Objeto Ticket.
     *
     * Persiste o objeto no banco de dados.
     */
    public void answerTicket(){
        super.getSelected().setDate(Calendar.getInstance().getTime());
        super.getSelected().setUser((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));
        super.getSelected().setTicket(ticketController.getSelected());
        ticketController.getSelected().getMessages().add(super.getSelected());
        super.save();
    }

    @Override
    public String setContextItems() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
}
