package Controller;

import Model.User;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "userController")
@ViewScoped
public class UserController extends AbstractController<User> {

    @Inject
    private HierarchyController hierarchyController;

    public UserController() {
        super(User.class);
    }
    
    /**
     * Ao ser instanciado, o objeto User.java tras consigo a lista de tickets vinculados a ele.
     * 
     * Quando o cliente está na pagina de cadastro de usuário e efetua uma requisição para
     * a pagina de cadastro de Ticket, a aplicação inclui no Map de requisição a lista de Objetos
     * Ticket contida no Objeto User para que possa ser recuperada em seguida por outro controlador.
     * 
     * O metodo navigateTicket() auxilia na transição dos dados entre as requsições.
     * 
     * @return link de navegação (outcome) para a pagina ticket
     */
    public String navigateTickets() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Ticket_items", this.getSelected().getTickets());
        }
        return "/index/ticket/index";
    }

    @Override
    public String setContextItems() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

}
