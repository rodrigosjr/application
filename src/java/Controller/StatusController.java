package Controller;

import Model.Status;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;

@Named(value = "statusController")
@ViewScoped
public class StatusController extends AbstractController<Status> {

    public StatusController() {
        super(Status.class);
    }

   /**
     * Ao ser instanciado, o objeto Status.java tras consigo a lista de Tickets vinculados a ele.
     * 
     * Quando o cliente está na pagina de cadastro de Status e efetua uma requisição para
     * a pagina de cadastro de Ticket, a aplicação inclui no Map de requisição a lista de Objetos
     * Ticket contida no Objeto Status para que possa ser recuperada em seguida por outro Objeto.
     * 
     * O metodo navigateTicket() auxilia na transição dos dados entre as requsições.
     * 
     * @return link de navegação (outcome) para a pagina Ticket
     */
    public String navigateTicket() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Ticket_items", this.getSelected().getTicket());
        }
        return "/index/ticket/index";
    }

    @Override
    public String setContextItems() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

}
