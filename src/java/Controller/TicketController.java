package Controller;

import Model.Ticket;
import Model.User;
import java.util.Calendar;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "ticketController")
@ViewScoped
public class TicketController extends AbstractController<Ticket> {

    @Inject
    private StatusController statusControlller;
    private UserController usersController;

    public UserController getUsersController() {
        return usersController;
    }

    public void setUsersController(UserController usersController) {
        this.usersController = usersController;
    }

    public TicketController() {
        super(Ticket.class);
    }

   /**
     * Ao ser instanciado, o objeto Ticket.java tras consigo a lista de mensagens vinculadas a ele.
     * 
     * Quando o cliente está na pagina de cadastro de Ticket e efetua uma requisição para
     * a pagina de cadastro de Mensagens, a aplicação inclui no Map de requisição a lista de Objetos
     * Message contida no Objeto Ticket para que possa ser recuperada em seguida por outro Objeto.
     * 
     * O metodo navigateMessages() auxilia na transição dos dados entre as requsições.
     * 
     * @return link de navegação (outcome) para a pagina Message
     */
    public String navigateMessages() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Message_items", this.getSelected().getMessages());
        }
        return "/index/message/index";
    }
    
    public void openTicket() {
        super.getSelected().setDateOpening(Calendar.getInstance().getTime());
        super.getSelected().setUser((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));
        super.getSelected().setStatus(statusControlller.getItems().stream().filter(k -> k.getDescription().equals("Aberto")).findAny().get());
        super.save();
    }

    @Override
    public String setContextItems() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

}
