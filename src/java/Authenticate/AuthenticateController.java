package Authenticate;

import Controller.UserController;
import Controller.util.Message;
import Model.User;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Rodrigo
 */
@Named(value = "authenticateController")
@ViewScoped
public class AuthenticateController implements Serializable {

    private UserController userController;

    public UserController getUserController() {
        return userController;
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    public User getUserContext() {
        return (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
    }

    public void setUserContext(User user) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
    }

    public void invalidateSession() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }

    public String accessValidation() {
        User user = this.findUser();
        if (user != null && this.userPasswordValidate(user)) {
            setUserContext(user);
            return "faces/index.xhtml?faces-redirect=true";
        }
        Message.addErrorMessage("Usuário ou Senha incorretos!");
        return null;
    }
    
    private User findUser() {
        Map<String, Object> filters = new HashMap<>();
        filters.put("login", userController.getSelected().getLogin());
        return userController.getItems(0, 0, null, null, filters).stream().findAny().orElse(null);
    }

    private Boolean userPasswordValidate(User userRegister) {
        return this.userController.getSelected().getPassword().equals(userRegister.getPassword());
    }
    
}
