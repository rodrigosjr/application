
package Authenticate;

import Model.User;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 *
 * @author Rodrigo
 */

public class AuthenticatePhaseListener implements PhaseListener {

    @Override
    public void afterPhase(PhaseEvent pe) {
        String page = pe.getFacesContext().getViewRoot().getViewId();
        User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        if (!page.contains("/authenticate.xhtml") && user == null) {
            pe.getFacesContext().getApplication().getNavigationHandler().handleNavigation(pe.getFacesContext(), null, "/authenticate.xhtml?faces-redirect=true");
        }
    }

    @Override
    public void beforePhase(PhaseEvent pe) {

    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

}
